import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import tvshows.Cartoon;
import tvshows.Documentary;
import tvshows.Drama;
import tvshows.RealityShow;
import tvshows.Show;

public class television {
	public static void main(String[] args){
		System.out.println("-----------------");
		System.out.println("Playing with Objects and Java Basics");
		System.out.println("-----------------");
		// Here we are using polymorphism declaring as the superclass 'Show'
		Show archer = new Cartoon("Archer", "FX");
		Show makemur = new Documentary("Making a Murderer", "Netflix");
		Show kards = new RealityShow("Keeping Up With the Kardashains", "E!");
		Show breakBad = new Drama("Breaking Bad", "AMC");
		Show got = new Drama("Game of Thrones", "HBO");
		
		// Initialize the information
		archer.rateMe((float) 8.8);
		archer.addViewers(1.5);
		((Cartoon) archer).getTagline("Sterling Archer, The World's Most Dangerous Spy");
		
		makemur.rateMe((float) 9.1);
		makemur.addViewers(3.3);
		((Documentary) makemur).getTagline("Reasonable doubt is for the innocent");
		((Documentary) makemur).getEvent("Trial of Steven Avery");
		
		kards.rateMe((float) 2.8);
		kards.addViewers(4.8);
		((RealityShow) kards).getTagline("There's Strength in Numbers");
		
		breakBad.rateMe((float) 9.5);
		breakBad.addViewers(4.32);
		((Drama) breakBad).getTagline("All Hail the King");
		
		got.rateMe((float) 9.5);
		got.addViewers(5.4);
		((Drama) got).getTagline("You Win or You Die");
		
		
		// Lets use arrays
		Show shows[] = new Show[5];
		shows[0] = archer;
		shows[1] = makemur;
		shows[2] = kards;
		shows[3] = breakBad;
		shows[4] = got;
		float temp = 0;
		int idx = 0;
		// Another way to implement loops (like C/C++)
		// This finds the highest rated program on the list
		for(int i = 0; i < 5; i++){
			if(shows[i].getRating() > temp){
				temp = shows[i].getRating();
				idx = i;
			}
		}
		System.out.println("Highest rated show: "+shows[idx].getName());
		System.out.println("Rating: " +shows[idx].getRating());
		
		// Cool java way of doing loops (Note: for java after 1.5)
		for(Show show : shows){
			show.printName();
			show.printTagline();
		}
		
		// Only Reality shows account for a loss of brain cells
		// But we have to declare it as a RealityShow
		RealityShow shore = new RealityShow("The Jersey Shore", "MTV");
		shore.setCellsLost(9);
		shore.addViewers(9);
		shore.loseBrainCells();
		System.out.println("Watching " +shore.getName());
		
		System.out.println("\n-----------------");
		System.out.println("Now play with other Collections");
		System.out.println("-----------------");

		
		// Lets try another type of Collection a Map makes sense here
		// Maps have Key/Value pairs in this case a name and rating
		Map<String, Float> myMap = new HashMap<>();
		System.out.println("Map: \nTitle : Rating");
		for(int i=0; i < 5; i++){
			myMap.put(shows[i].getName(), shows[i].getRating());
		}
		for(String i : myMap.keySet()){
			System.out.println(i + " : " + myMap.get(i));
		}
		
		// Lets use a set so we can make use of sorted names
		System.out.println("\nSets: ");
		Set<String> mySet = new TreeSet<>(); // Here used a Tree Set unlike the example
		for(int i=0; i < 5; i++){
			mySet.add(shows[i].getName());
		}
		for(String i : mySet){
			System.out.println(i);
		}
		
		System.out.println("\n-----------------");
		System.out.println("Now play with Pointers");
		System.out.println("-----------------");
		// No we want to do some work with pointers
		// Edit some of the code provided by Lee
		passByValue();
		passReferencesByValue();
		useEquals();
		
		System.out.println("\n-----------------");
		System.out.println("Now play with Exceptions");
		System.out.println("-----------------");
	
		// Exceptions
		Show rick = new Cartoon("Rick and Morty","Adult Swim");
		try{
			throwException(rick.getRating());
		}
		catch(Exception e){
			System.err.println(e.getMessage());
			System.out.println("I got an exception! Now I have to handle it!");
			rick.rateMe((float)9.3);
		}
		
		System.out.println(rick.getName() + ": " + rick.getRating());
	}
	
	
	private static void passByValue() {
		Show wire = new Drama("The Wire","HBO");
		foo(wire);
	
		if (wire.getName().equals("The Wire")) { //true
			System.out.println( "Java passes by value." );
	
		} else if (wire.getName().equals("Friends")) {
			System.out.println( "Java passes by reference." );
		}
	}

	private static void foo(Show d) {
		d.getName().equals("The Wire"); // true
	
		d = new Drama("Friends","TBS");
		d.getName().equals("Friends"); // true
	}

	private static void passReferencesByValue() {
		Show wire = new Drama("The Wire","HBO");
		bar(wire);
		if (wire.getName().equals("The Wire")) { //true
			System.out.println( "Java passes copy of value as the value!" );

		} else if (wire.getName().equals("Friends")) {
			System.out.println( "Java passes copy of reference as the value!" );
		}
		
	}

	private static void bar(Show d) {
		d.rename("Friends");
	}

	private static void useEquals() {
		String str1 = "Does This Work?";
		String str2 = "Does This Work?";
		if(str1==str2){
			System.out.println("Use == when comparing objects.");
		}
		if(str1.equals(str2)){
			System.out.println("Always Use .equals() when comparing objects!");
		}
		
	}
	
	private static void throwException(float f) throws ArithmeticException{
		if(f == 0){
			throw new ArithmeticException("This show is not yet Rated. Give it a rating.");
		}
	}
}
