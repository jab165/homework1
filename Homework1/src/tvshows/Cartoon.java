package tvshows;

public class Cartoon extends Show{
	private String tagline;
	
	public Cartoon(String name, String net){
		super(name, net);
		tagline = "No Current Tagline";
	}

	public void getTagline(String tag){
		this.tagline = tag;
	}
	
	public void printTagline() {
		// TODO Auto-generated method stub
		System.out.println("\"" +this.tagline+ "\"");
	}	
}

