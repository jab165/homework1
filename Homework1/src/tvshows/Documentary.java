package tvshows;

public class Documentary extends Show{

	private String tagline;
	private String event;
	
	public Documentary(String name, String net){
		super(name,net);
		this.tagline = "No Tagline Set";
		this.event = "No event set";
	}
	
	public void getTagline(String tag){
		this.tagline = tag;
	}
	
	public void getEvent(String e){
		this.event = e;
	}
	

	public void printTagline() {
		// TODO Auto-generated method stub
		System.out.println("\"" +this.tagline+ "\"");
		System.out.println("This is documenting: " + this.event);
	}

}
