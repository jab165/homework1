package tvshows;
//This is an abstract class that will be inherited by the different
//types of tv shows
public abstract class Show {
	protected String name;
	protected String network;
	protected double viewers;
	protected float rating;
	
	public Show(String name, String net){
		this.name = name;
		this.network = net;
		this.viewers = 0;
		this.rating = 0;
	}
	
	public String getName(){
		return this.name;
	}

	public void rateMe(float r){
		this.rating = r;
	}
	
	public float getRating(){
		return this.rating;
	}
	
	public String getNetwork(){
		return this.network;
	}
	
	public void addViewers(double eyes){
		this.viewers += eyes;
	}
	
	public void rename(String s){
		this.name = s;
	}
	
	public double returnViewers(){
		return this.viewers;
	}
	
	public void printName(){
		System.out.println(name);
	}

	public abstract void printTagline();
}
